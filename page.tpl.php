<?php 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>
<title><?php print $head_title; ?></title>

<?php print $head ?>
<?php print $styles ?>

	<!--[if lte IE 7]>
	<style type="text/css" media="all">@import "<?php print base_path() . path_to_theme() ?>/ie-fix.css";</style>
    
    <![endif]-->

<?php print $scripts ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

</head>

<body>
<!-- POPUP --><!-- script src=http://www.mwolk.com/popup.popup/script.js></script --><!-- END -->
<?php  if ($abs_top) { print $abs_top; }?>
<div id="page_wrapper">
  <div id="header">
  <div id="logo">
<?php  
//this trick allows to show transparent .png in IE 5.5 and 6
// make sure that we are only replacing PNG logo for the Windows versions of Internet
// Explorer 5.5+
$msie='/msie\s(5\.[5-9]|[6]\.[0-9]*).*(win)/i';
if( !isset($_SERVER['HTTP_USER_AGENT']) ||
!preg_match($msie,$_SERVER['HTTP_USER_AGENT']) ||
preg_match('/opera/i',$_SERVER['HTTP_USER_AGENT'])) { ?>

        <a href="<?php print base_path() ?>" title="<?php print t('Home') ?>"><img src="<?php print base_path() . path_to_theme(); ?>/img/logo.png" alt="<?php print t('Home') ?>" /></a>

<?php } 

else { ?> 

<!-- MSIE Alpha Transparency work around - not needed on decent browsers -->

<a href="<?php print base_path() ?>" title="<?php print t('Home') ?>">

<img src="<?php print base_path() . path_to_theme(); ?>/img/blank.gif" style="width: 249px; height: 95px; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?php print base_path() . path_to_theme(); ?>/img/logo.png', sizingMethod='scale');" alt="<?php print t('Home') ?>" /></a>

<?php } ?>
        
        
      </div>
      
      <?php if (isset($secondary_links)) { ?>
      <div id="secondary"><?php print theme('links', $secondary_links) ?></div>
      <?php } ?>
	  	  
	  
 
  
   
  </div>
  <div id="search_block">
  
  <?php if (isset($primary_links)) { ?>
      <div id="primary"><?php print theme('links', $primary_links) ?></div>
      <?php } ?>
      
  <?php print $search_box ?> 
  
  </div>
  
  
  <div id="container">
    <div id="center" class="column">
<?php  print $header;?>
      <div id="main"> 
        
		
        <?php if ($title && !$is_front) {  ?>
        <h1 class="title"><?php print $title ?></h1>
        <?php } ?>
		
        <?php if ($tabs) { ?>
        <div class="tabs"><?php print $tabs ?></div>
        <?php } ?>
        <?php print $help ?> <?php print $messages ?> 

	  <?php  if ($content_top) { print $content_top; }?>	
		<?php print $content; ?> </div>
      
    </div>
	
  <div id="left" class="column">      
      <?php  if ($sidebar_left) { 
   print $sidebar_left;
   }  ?>
   
    </div> 
	
    <div id="right" class="column">
      <?php if ($sidebar_right) { 
   print $sidebar_right;
    } ?>
	
    </div>
  </div>

    <div id="footer"> 

      
      <?php print $footer_message ?> 
	  
  <?php print $closure ?>
  
  </div>
		<div style="width: 100px; float: right; font-size: 80%; text-align: right; padding-right: 10px;">
  	<a href="http://mwolk.com/blog" style="color: gray;">Mwolk</a>
  	</div>
  <!--- end -->
</div>
</body>
</html>
